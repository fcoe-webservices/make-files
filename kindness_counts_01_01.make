core: 8.x
api: 2
projects:
  drupal:
    version: '8.7.3'
  admin_toolbar:
    version: '1.27'
  google_analytics:
    version: '2.4'
  video:
    version: '1.4'
  webform:
    version: '5.2'
  webform_views:
    version: '5.0-alpha7'
  zurb_foundation:
    version: '6.0-alpha5'
  kindness_counts:
    type: 'theme'
    download:
      type: 'git'
      url: 'git@bitbucket.org:fcoe-webservices/kindness_counts.git'
