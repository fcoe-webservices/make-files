core = 7.x
api = 2

projects[drupal][version] = "7.91"


projects[admin_menu][version] = "3.0-rc6"

projects[auto_entitylabel][version] = "1.4"

projects[bundle_copy][version] = "1.1"

projects[comment_alter][version] = "1.x-dev"

projects[comment_goodness][version] = "1.4"

projects[comment_perm][version] = "1.0"

projects[common_body_class][version] = "1.1"

projects[computed_field][version] = "1.1"

projects[conditional_fields][version] = "3.0-alpha2"

projects[rules_conditional][version] = "1.0"

projects[context][version] = "3.11"

projects[context_profile_role][version] = "1.1"

projects[ctools][version] = "1.20"

projects[date][version] = "2.12"

projects[diff][version] = "3.4"

projects[draggableviews][version] = "2.1"

projects[editor_note][version] = "1.7"

projects[entity][version] = "1.10"

projects[entityform][version] = "2.0-rc4"

projects[entityreference][version] = "1.5"

projects[entityreference_filter][version] = "1.7"

projects[entityreference_prepopulate][version] = "1.8"

projects[eva][version] = "1.4"

projects[extended_file_field][version] = "1.0"

projects[features][version] = "2.14"

projects[feeds][version] = "2.0-beta5"

projects[feeds_entity_processor][version] = "1.x-dev"

projects[field_group][version] = "1.6"

projects[field_permissions][version] = "1.1"

projects[filefield_paths][version] = "1.1"

projects[flag][version] = "3.9"

projects[flatcomments][version] = "2.0"

projects[hide_submit][version] = "2.4"

projects[insert][version] = "1.4"

projects[job_scheduler][version] = "2.0"

projects[jquery_update][version] = "2.7"

projects[libraries][version] = "2.5"

projects[logintoboggan][version] = "1.5"

projects[mailsystem][version] = "2.35"

projects[markup][version] = "1.3"

projects[masquerade][version] = "1.0-rc7"

projects[mass_password_change][version] = "1.0-beta1"

projects[menu_views][version] = "2.5"

projects[mimemail][version] = "1.2"

projects[module_filter][version] = "2.2"

projects[nocurrent_pass][version] = "1.1"

projects[og][version] = "2.10"

projects[pathauto][version] = "1.3"

projects[prepopulate][version] = "2.x-dev"

projects[quicktabs][version] = "3.8"

projects[registration][version] = "1.7"

projects[relation][version] = "1.2"

projects[relation_add][version] = "1.7"

projects[remove_field_label_length_limit][version] = "1.1"

projects[rules][version] = "2.13"

projects[rules_link][version] = "2.0"

projects[special_menu_items][version] = "2.1"

projects[smtp][version] = "2.x-dev"

projects[token][version] = "1.9"

projects[user_import][version] = "3.2"

projects[view_profiles_perms][version] = "1.0"

projects[views][version] = "3.25"

projects[views_bulk_operations][version] = "3.6"

projects[views_conditional][version] = "1.3"

projects[views_data_export][version] = "3.2"

projects[views_field_view][version] = "1.2"

projects[views_flipped_table][version] = "1.1"

projects[views_pivot][version] = "1.1"

projects[views_role_based_global_text][version] = "1.1"

projects[views_rules][version] = "1.0"

projects[year_field][version] = "1.0"

projects[entityform_noe][type] = "module"
projects[entityform_noe][download][type] = "git"
projects[entityform_noe][download][url] = "git@bitbucket.org:fcoe-webservices/entityform_noe.git"
projects[entityform_noe][branch] = "master"
projects[entityform_noe][subdir] = "custom/features"

projects[cancel_button][download][type] = "git"
projects[cancel_button][download][url] = "git@bitbucket.org:fcoe-webservices/cancel_button.git"
projects[cancel_button][type] = "module"
projects[cancel_button][subdir] = "custom"

projects[disable_induction_fields][download][type] = "git"
projects[disable_induction_fields][download][url] = "git@bitbucket.org:fcoe-webservices/disable-induction-fields.git"
projects[disable_induction_fields][type] = "module"
projects[disable_induction_fields][subdir] = "custom"

projects[disable_monthlylog_fields][download][type] = "git"
projects[disable_monthlylog_fields][download][url] = "git@bitbucket.org:fcoe-webservices/disable-monthlylog-fields.git"
projects[disable_monthlylog_fields][type] = "module"
projects[disable_monthlylog_fields][subdir] = "custom"

projects[edit_link][download][type] = "git"
projects[edit_link][download][url] = "git@bitbucket.org:fcoe-webservices/edit_link.git"
projects[edit_link][type] = "module"
projects[edit_link][subdir] = "custom"

projects[limit_profile_pic][download][type] = "git"
projects[limit_profile_pic][download][url] = "git@bitbucket.org:fcoe-webservices/limit_profile_pic.git"
projects[limit_profile_pic][type] = "module"
projects[limit_profile_pic][subdir] = "custom"

projects[username_privacy][download][type] = "git"
projects[username_privacy][download][url] = "git@bitbucket.org:fcoe-webservices/username_privacy.git"
projects[username_privacy][type] = "module"
projects[username_privacy][subdir] = "custom"


projects[induction_foundation][download][type] = "git"
projects[induction_foundation][download][url] = "git@bitbucket.org:fcoe-webservices/induction_foundation.git"
projects[induction_foundation][type] = "theme"
