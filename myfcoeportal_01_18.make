core = 7.x
api = 2

; Core
projects[drupal][version] = "7.89"

; Modules
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc6"

projects[content_access][subdir] = "contrib"
projects[content_access][version] = "1.2-beta3"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.20"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.10"

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.5"

projects[feeds][subdir] = "contrib"
projects[feeds][version] = "2.0-beta5"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.6"

projects[flood_control][subdir] = "contrib"
projects[flood_control][version] = "1.x-dev"

projects[grammar_parser_lib][subdir] = "contrib"
projects[grammar_parser_lib][version] = "1.1"

projects[honeypot][subdir] = "contrib"
projects[honeypot][version] = "1.26"

projects[job_scheduler][subdir] = "contrib"
projects[job_scheduler][version] = "2.0"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.5"

projects[link][subdir] = "contrib"
projects[link][version] = "1.9"

projects[login_destination][subdir] = "contrib"
projects[login_destination][version] = "1.4"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.2"

projects[og][subdir] = "contrib"
projects[og][version] = "2.10"

projects[og_extras][subdir] = "contrib"
projects[og_extras][version] = "1.2"

projects[references][subdir] = "contrib"
projects[references][version] = "2.2"

projects[smtp][subdir] = "contrib"
projects[smtp][version] = "2.x-dev"

projects[user_import][subdir] = "contrib"
projects[user_import][version] = "3.2"

projects[views][subdir] = "contrib"
projects[views][version] = "3.25"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.6"

projects[webform][subdir] = "contrib"
projects[webform][version] = "4.24"

; Themes
projects[zurb_foundation][version] = "5.0-rc8"

projects[fcoeportal][download][type] = "git"
projects[fcoeportal][download][url] = "git@bitbucket.org:fcoe-webservices/portal-7"
projects[fcoeportal][type] = "theme"

projects[mresource][download][type] = "git"
projects[mresource][download][url] = "git@bitbucket.org:fcoe-webservices/mresource-7.git"
projects[mresource][type] = "module"

projects[user_import_og][download][type] = "git"
projects[user_import_og][download][url] = "git@bitbucket.org:fcoe-webservices/user-import-og.git"
projects[user_import_og][type] = "module"
