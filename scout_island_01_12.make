core = 7.x

api = 2
projects[drupal][version] = "7.91"

; Modules

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc6"

projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.6"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.20"

projects[getdirections][subdir] = "contrib"
projects[getdirections][version] = "2.4"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.7"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.5"

projects[location][subdir] = "contrib"
projects[location][version] = "3.7"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.3"

projects[smtp][subdir] = "contrib"
projects[smtp][version] = "2.x-dev"

projects[token][subdir] = "contrib"
projects[token][version] = "1.7"

projects[views][subdir] = "contrib"
projects[views][version] = "3.25"

projects[views_galleriffic][subdir] = "contrib"
projects[views_galleriffic][version] = "1.1"

projects[weather][subdir] = "contrib"
projects[weather][version] = "3.1"

projects[webform][subdir] = "contrib"
projects[webform][version] = "3.31"

; Themes

projects[marinelli][version] = "3.0-beta12"

projects[scout_marinelli][download][type] = "git"
projects[scout_marinelli][download][url] = "git@bitbucket.org:fcoe-webservices/scout-marinelli.git"
projects[scout_marinelli][type] = "theme"
projects[scout_marinelli][directory_name] = "scout_marinelli"