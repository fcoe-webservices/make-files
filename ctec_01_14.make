core: 8.x
api: 2
projects:
  drupal:
    version: '8.9.19'
  admin_toolbar:
    version: '2.5'
  auto_entitylabel:
    version: '3.0-beta4'
  ctools:
    version: '3.7'
  config_update:
    version: '1.7'
  contact_block:
    version: '1.5'
  crop:
    version: '1.5'
  gdoc_field:
    version: '1.1'
  entityqueue:
    version: '1.2'
  features:
    version: '3.12'
  foundation_layouts:
    version: '6.0'
  fullcalendar_view:
    version: '2.7'
  insert:
    version: '2.0-beta2'
  layout_builder_browser:
    version: '1.1'
  layout_builder_modal:
    version: '1.1'
  # IMPORTANT
  # Layout Builder Modal must be kept at 1.1 at this
  # time. Version 1.2 broke the update of the site

  layout_builder_restrictions:
    version: '2.8'
  media_library_theme_reset:
    version: '1.0'
  menu_block:
    version: '1.7'
  pathauto:
    version: '1.8'
  photoswipe:
    version: '2.9'
  redirect_after_login:
    version: '2.7'
  smart_read_more_link:
    version: '1.7'
  token:
    version: '1.9'
  twig_tweak:
    version: '2.9'
  user_default_page:
    version: '2.0-rc1'
  views_field_formatter:
    version: '1.13'
  zurb_foundation:
    version: '6.0-alpha6'


  ludwig:
    version: '1.8'
  smtp:
    version: '1.0'


  ctecclean:
    type: 'theme'
    download:
      type: 'git'
      url: 'git@bitbucket.org:fcoe-webservices/ctec-clean.git'


  # IMPORTANT
  # File URL module must be added manualy or copied over from a previous 
  # platform because it cannot be added via a makefile.