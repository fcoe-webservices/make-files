core = 7.x

api = 2
projects[drupal][version] = "7.91"


; Modules
==================================================

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc6"

projects[addressfield][version] = "1.3"
projects[addressfield][subdir] = "contrib"

projects[backgroundfield][version] = "1.x-dev"
projects[backgroundfield][subdir] = "contrib"
projects[backgroundfield][patch] = https://www.drupal.org/files/backgroundfield_undefined_index-1841978-18.patch
; Issue with undefined index error message
; See issue queue at https://www.drupal.org/node/1841978#comment-11653383

projects[block_class][version] = "2.3"
projects[block_class][subdir] = "contrib"

projects[classy_paragraphs][version] = "1.0"
projects[classy_paragraphs][subdir] = "contrib"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.20"

projects[conditional_fields][subdir] = "contrib"
projects[conditional_fields][version] = "3.0-alpha2"

projects[custom_add_another][subdir] = "contrib"
projects[custom_add_another][version] = "1.1"

projects[date][subdir] = "contrib"
projects[date][version] = "2.12"

projects[email][subdir] = "contrib"
projects[email][version] = "1.3"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.10"

projects[entityform][subdir] = "contrib"
projects[entityform[version] = "2.0-rc4"

projects[entityform_block][subdir] = "contrib"
projects[entityform_block][version] = "1.3"

projects[entity_rules][subdir] = "contrib"
projects[entity_rules][version] = "1.0-alpha4"

projects[entity2text][subdir] = "contrib"
projects[entity2text][version] = "1.0-alpha3"

projects[features][version] = "2.14"
projects[features][subdir] = "contrib"

projects[field_collection][subdir] = "contrib"
projects[field_collection][version] = "1.2"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.6"

projects[field_permissions][subdir] = "contrib"
projects[field_permissions][version] = "1.1"

projects[filefield_paths][subdir] = "contrib"
projects[filefield_paths][version] = "1.1"

projects[genpass][subdir] = "contrib"
projects[genpass][version] = "1.1"

projects[insert][subdir] = "contrib"
projects[insert][version] = "1.4"

projects[jquery_update][version] = "2.7"
projects[jquery_update][subdir] = "contrib"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.5"

projects[link][subdir] = "contrib"
projects[link][version] = "1.11"

projects[markup][subdir] = "contrib"
projects[markup][version] = "1.3"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.2"

projects[multiple_selects][subdir] = "contrib"
projects[multiple_selects][version] = "1.2"

projects[paragraphs][version] = "1.0-rc5"
projects[paragraphs][subdir] = "contrib"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.3"

projects[realname][subdir] = "contrib"
projects[realname][version] = "1.4"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.13"

projects[select_or_other][subdir] = "contrib"
projects[select_or_other][version] = "2.24"

projects[smtp][subdir] = "contrib"
projects[smtp][version] = "2.x-dev"

projects[token][subdir] = "contrib"
projects[token][version] = "1.9"

projects[views][subdir] = "contrib"
projects[views][version] = "3.25"

projects[viewfield][subdir] = "contrib"
projects[viewfield][version] = "2.2"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.6"

projects[views_data_export][subdir] = "contrib"
projects[views_data_export][version] = "3.2"

projects[views_slideshow][subdir] = "contrib"
projects[views_slideshow][version] = "3.10"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.9"

projects[classy_paragraphs_ui][type] = "module"
projects[classy_paragraphs_ui][download][type] = "git"
projects[classy_paragraphs_ui][download][url] = "https://github.com/dervishmoose/classy_paragraphs_ui.git"
projects[classy_paragraphs_ui][download][revision] = "e27aba21259e61a27cc8dde321e348dc5fe3eb3d"

; Features
==================================================
projects[features_ct_reflection][download][type] = "git"
projects[features_ct_reflection][download][url] = "git@bitbucket.org:fcoe-webservices/paragraph_bundles.git"
projects[features_ct_reflection][type] = "module"
projects[features_ct_reflection][subdir] = "custom/features"
projects[features_ct_reflection][directory_name] = "paragraph_bundles"


; Themes
==================================================

projects[zen][version] = "5.6"

projects[smefair_zen][type] = "theme"
projects[smefair_zen][download][type] = "git"
projects[smefair_zen][download][url] = "git@bitbucket.org:fcoe-webservices/smefair-zen.git"


projects[specialevents][type] = "theme"
projects[specialevents][download][type] = "git"
projects[specialevents][download][url] = "git@bitbucket.org:fcoe-webservices/special-events-theme.git"
projects[specialevents][directory_name] = "specialevents"


; Libraries
==================================================

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"