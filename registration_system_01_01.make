core = 7.x
api = 2

; Core
projects[drupal][version] = "7.87"

; Defaults
defaults[projects][subdir] = "contrib"

; Modules
projects[admin_menu][version] = "3.0-rc6"

projects[ctools][version] = "1.20"

projects[date][version] = "2.12"

projects[email][version] = "1.3"

projects[entity][version] = "1.10"

projects[entityreference][version] = "1.5"

projects[jquery_update][version] = "2.7"

projects[libraries][version] = "2.5"

projects[module_filter][version] = "2.2"

projects[pathauto][version] = "1.3"

projects[registration][version] = "1.7"

projects[rules][version] = "2.13"

projects[smtp][version] = "2.x-dev"

projects[token][version] = "1.9"

projects[views][version] = "3.25"

projects[views_aggregator][version] = "1.4"

projects[views_bulk_operations][version] = "3.6"

projects[views_calc][version] = "1.1"

projects[views_data_export][version] = "3.2"


;Themes
projects[zurb-foundation][version] = "4.0"

